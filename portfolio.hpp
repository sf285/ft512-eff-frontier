#ifndef __PORTFOLIO_HPP__
#define __PORTFOLIO_HPP__


#include <cstdlib>
#include <cstdio>
#include <string>
#include <vector>
#include <iostream>
#include <Eigen/Dense>
using namespace std;
using namespace Eigen;

class Portfolio{
	public:
		int ass_num;
		MatrixXd corr;
		VectorXd weight;
		vector<double> a_aver;
		vector<double> a_sd;
		double p_sd;
		
		void cal_sd();
};

#endif

