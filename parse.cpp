#include "parse.hpp"

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <cmath>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;


vector<string> split(string s){
	if(s.size()==0){
		cerr<<"the EMPTY LINE"<<endl;
                        exit(EXIT_FAILURE);
        }
	vector<string> str;
	string cur_str;
	size_t ind=0;
	size_t start=0;
	while((ind=s.find(",",ind))!= string::npos){
		if(start==0){
			cur_str=s.substr(start,ind-start);
			str.push_back(cur_str);
			start=ind;
			++ind;
		}else{
			cur_str=s.substr(start+1,ind-start);
                        str.push_back(cur_str);
                        start=ind;
                        ++ind;
		}
	}
	cur_str=s.substr(start+1,s.size());
	str.push_back(cur_str);
	return str;
}

void readUni(ifstream &f1, Portfolio &port){
	//vector<Asset> asset;
	string line;
	vector<string> str_l;
	size_t len=0;
	while (getline(f1,line)){
		str_l=split(line);
		if(str_l.size()!=3){
			cerr<<"the line input of sd/ror is in wrong pattern"<<endl;
                        exit(EXIT_FAILURE);
                }
		char* sr;
		char* sc;
			
		double av=strtod(str_l[1].c_str(),&sr);
		double s=strtod(str_l[2].c_str(),&sc);
		if(strlen(sr)==str_l[1].size() || strlen(sc)==str_l[2].size()){
			cerr<<"the input of sd/ror is in wrong pattern"<<endl;
			exit(EXIT_FAILURE);
		}
		if (s<0){
			cerr<<"the input value of sd is in wrong pattern"<<endl;
                        exit(EXIT_FAILURE);
                }

		port.a_aver.push_back(av);
		port.a_sd.push_back(s);
		len++;
	}
	port.ass_num = len;
}

void readCor(ifstream &f2,Portfolio &port){
	size_t num=port.ass_num;
	MatrixXd cor_m(num,num);
	string line;
	size_t ind_r=0;
//	size_t start=0;
	vector<string> str_l;
	size_t len=0;
//	cout<<num;
	while (getline(f2,line)){
		str_l=split(line);
		if(str_l.size()!=num){
			cerr<<"the number of elements in correlation is in wrong pattern"<<endl;
                        exit(EXIT_FAILURE);
                }
		for (size_t i = 0; i < num; i ++){
			char *co;
			double cor=strtod(str_l[i].c_str(),&co);
			if(strlen(co)==str_l[i].size()){
               	        	cerr<<"the input of correlation is in wrong pattern"<<endl;
                	        exit(EXIT_FAILURE);
                	}
			if(cor>1 || cor<-1){
				cerr<<"the input of correlation is over bound"<<endl;
                                exit(EXIT_FAILURE);
                        }

			cor_m(ind_r,i)=cor;
		}
		ind_r++;
		len++;
	}
	if(len!=num){
		 cerr<<"Matrix of Correlation in wrong pattern"<<endl;
                        exit(EXIT_FAILURE);
        }
//	cout<<cor_m(7,3)<<"wqer"<<endl;
//	cout<<cor_m.rows()<<" "<<cor_m.cols()<<endl;
	//we wanna check correlation in wright pattern
//	cout<<cor_m<<endl;
	for (size_t x=0; x<num; x++){
		for (size_t y=0; y<num; y++){
			if(cor_m(x,y)!=cor_m(y,x)){
		//		cout<<cor_m(x,y)<<" "<<cor_m(y,x)<<endl;
				cerr<<"the cor (x,y)!=cor (y,x)"<<endl;
                        	exit(EXIT_FAILURE);
                	}
			if(x==y){
				if(cor_m(x,y)!=1){
					cerr<<"the cor (x,x)!= 1"<<endl;
	                                exit(EXIT_FAILURE);
        	                }
			}
		}
	}
	port.corr=cor_m;
}

MatrixXd covariance(int num,Portfolio &p){
	MatrixXd cov(num,num);
        for (int i = 0; i < num; i++){
                for(int j = 0; j < num; j++){
                        cov(i,j)=p.a_sd[i] * p.a_sd[j]*p.corr(i,j);
                }
        }
	return cov;
}

void unrestOp(Portfolio & port, double r){
	int num= port.ass_num;
	//get the covariance
	MatrixXd cov=covariance(num,port);


	//create matrix a
	MatrixXd a1 = MatrixXd::Ones(1,num);
	MatrixXd a2(1,num);
	for (int i = 0; i < num ;i++){
		a2(i)=port.a_aver[i];
	}
	MatrixXd A(2,num);
	A<<a1,a2;

	//create matrix b
	MatrixXd b1 = MatrixXd::Zero(num,1);
	MatrixXd b2(2,1);
	b2<<1,r;
	MatrixXd B(num+2,1);
	B<<b1,b2;
	
	//create zero function
	MatrixXd Z = MatrixXd::Zero(2,2);

	//left side matrix 
	MatrixXd R(num+2,num+2);
	R<<cov,A.transpose(),A,Z;

	//our solutions
	VectorXd Sol(num+2,1);

	//use eigen function to solve the equations
	Sol=R.fullPivHouseholderQr().solve(B);

	//get our unrestricted weight
	MatrixXd we_m(num,1);
	for (int u = 0; u < num; u++){
		we_m(u,0)=Sol(u,0);
	}
	port.weight=we_m;
	port.cal_sd();
}

int findNegMin(MatrixXd cur_we,int num){
	double cur_v=0.0;
	int ind=-1;
	for(int i =0; i < num; i++){
		if(cur_v>cur_we(i,0)){
			cur_v=cur_we(i,0);
			ind=i;
		}
	}
	return ind;
}



void restOp(Portfolio &port, double r){
	int num= port.ass_num;
        //get the covariance
        MatrixXd cov=covariance(num,port);

	//create A matrix 
        MatrixXd a1 = MatrixXd::Ones(1,num);
        MatrixXd a2(1,num);
        for (int i = 0; i < num ;i++){
                a2(i)=port.a_aver[i];
        }
        MatrixXd A(2,num);
        A<<a1,a2;

        //create B  matrix 
        MatrixXd b1 = MatrixXd::Zero(num,1);
        MatrixXd b2(2,1);
        b2<<1,r;
        MatrixXd B(num+2,1);
        B<<b1,b2;

	//create zero function 
        MatrixXd Z = MatrixXd::Zero(2,2);

	//left side matrix 
        MatrixXd R(num+2,num+2);
        R<<cov,A.transpose(),A,Z;

	//our solutions 
        VectorXd Sol(num+2,1);

	//use eigen function to solve the equations
        Sol=R.fullPivHouseholderQr().solve(B);

	//get weight 
        MatrixXd we_m(num,1);
        for (int u = 0; u < num; u++){
                we_m(u,0)=Sol(u,0);
        }
        port.weight=we_m;
        port.cal_sd();

	//cout<<we_m<<endl;
	int check = 0;
//	int cur_ch = 0;
	int count = 0;
	for(int i =0; i < num; i++){
		check=findNegMin(we_m,num);
	//	cout<<check<<endl;
	//	cout<<we_m<<endl;
		if(check != -1){
			count++;
			MatrixXd ZZ=MatrixXd::Zero(count+2,count+2);
			MatrixXd a3=MatrixXd::Zero(1,num);
        	        a3(0,check) = 1;
			A.conservativeResize(A.rows()+1,A.cols());
			A.row(A.rows()-1)=a3;
			//cout<<a3<<endl;
			b2.conservativeResize(b2.rows()+1,b2.cols());
			b2(b2.rows()-1,0)=0;
			B.conservativeResize(B.rows()+1,B.cols());
			B<<b1,b2;
			MatrixXd RR(num+2+count,num+2+count);
			RR<<cov,A.transpose(),A,ZZ;
			MatrixXd Sol(num+2+count,1);
			Sol=RR.fullPivHouseholderQr().solve(B);
			for (int u = 0; u < num; u++){
		                we_m(u)=Sol(u);
        		}
			
		//	we_m=Sol.head(num);
		}
	}

	port.weight=we_m;
        port.cal_sd(); 
//	cout<<port.p_sd<<endl;
}

