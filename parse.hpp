#ifndef __PARSE_HPP__
#define __PARSE_HPP__

#include "portfolio.hpp"

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <cmath>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;

void restOp(Portfolio &port, double r);
int findNegMin(MatrixXd cur_we,int num);
void unrestOp(Portfolio & port, double r);
MatrixXd covariance(int num,Portfolio &p);
void readCor(ifstream &f2,Portfolio &port);
void readUni(ifstream &f1, Portfolio &port);
vector<string> split(string s);

#endif


