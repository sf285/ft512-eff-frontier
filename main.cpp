#include "parse.hpp"
#include "portfolio.hpp"

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <unistd.h>
//#include <Eigen/Dense>
#include <iomanip>
using namespace Eigen;
using namespace std;

int main(int argc, char **argv){
	if(argc != 3 && argc != 4){
		cerr<<"The Number of Input is wrong"<<endl;
		return EXIT_FAILURE;
	}

	int check;
	bool r = false;
	while ((check = getopt(argc, argv, "r")) != -1){
   		 switch(check){
    			case 'r':
        			r = true;
       				break;
      			case '?':
        			cerr<<"Option is wrong";
        			return EXIT_FAILURE;
    		}
  	}
	ifstream f1;
	ifstream f2;
	if(r==true){
		f1.open(argv[2]);
		if(!f1){
			cerr<<"Cannot Open File-Universe."<<endl;
			return EXIT_FAILURE;
		}
        	f2.open(argv[3]);
		if(!f2){
                	cerr<<"Cannot Open File-Correlation."<<endl;
                	return EXIT_FAILURE;
        
		}
	}else{
                f1.open(argv[1]);
                if(!f1){
                        cerr<<"Cannot Open File-Universe."<<endl;
                        return EXIT_FAILURE;
                }
                f2.open(argv[2]);
                if(!f2){
                        cerr<<"Cannot Open File-Correlation."<<endl;
                        return EXIT_FAILURE;
		}
	}
	
	Portfolio P;
	readUni(f1,P);
//	cout<<P.corr<<endl;
	readCor(f2,P);
//	double res=0.0;
	if(r == true){
		for (double i=0.01; i<0.263;i+=0.01){
			restOp(P,i);
			if(i==0.01){
				cout<<"ROR,volatility"<<endl;
				cout<<fixed<<setprecision(1)<<i*100<<"%, ";
				cout<<fixed<<setprecision(2)<<P.p_sd*100<<"%"<<endl;
			}else{
				cout<<fixed<<setprecision(1)<<i*100<<"%, ";
                                cout<<fixed<<setprecision(2)<<P.p_sd*100<<"%"<<endl;
			}	
		}	
	}
	if(r == false){
		for (double i=0.01; i<0.263;i+=0.01){
			unrestOp(P,i);
                	if(i==0.01){
				cout<<"ROR,volatility"<<endl;
				cout<<fixed<<setprecision(1)<<i*100<<"%, ";
                        	cout<<fixed<<setprecision(2)<<P.p_sd*100<<"%"<<endl;
			}else{
				cout<<fixed<<setprecision(1)<<i*100<<"%, ";
                                cout<<fixed<<setprecision(2)<<P.p_sd*100<<"%"<<endl;
			}
		}
	}
	cout.unsetf(std::ios::fixed);
	return EXIT_SUCCESS;

		
}

